<?php
    /**
     * Created by PhpStorm.
     * User: jacksonfdam
     * Date: 2021-04-12
     * Time: 10:52
     */
    
    namespace App\Exception;

    use Exception;
    use Psr\Container\ContainerExceptionInterface;
    
    /**
     * Class could not be instantiated
     */
    class ContainerException extends Exception implements ContainerExceptionInterface
    {
    }
