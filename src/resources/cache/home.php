<?php class_exists('App\Lib\View') or exit; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">

    <title>Wunder Mobility - Fleet</title>

    <meta content="Wunder Mobility" name="description">
    <meta content="Jackson" name="author">

    <link href="assets/css/bootstrap.min.css?bust=<?php echo $bust ?>" rel="stylesheet">
    <link href="assets/css/style.css?bust=<?php echo $bust ?>" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css?bust=<?php echo $bust ?>"
          rel="stylesheet">

</head>
<body>

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-11 col-sm-9 col-md-7 col-lg-6 col-xl-5 text-center p-0 mt-3 mb-2">
            <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                <img alt="Wunder Mobility" src="assets/img/wunder-logo-2020.svg" title="Wunder Mobility"/>

                <h2 id="heading">Sign Up Your User Account</h2>
                <p>Fill all form field to go to next step</p>
                <form id="msform">
                    <!-- progressbar -->
                    <ul id="progressbar">
                        <li class="active" id="personal"><strong>Personal</strong></li>
                        <li id="address"><strong>Address</strong></li>
                        <li id="payment"><strong>Payment</strong></li>
                        <li id="confirm"><strong>Finish</strong></li>
                    </ul>
                    <div class="progress">
                        <div aria-valuemax="100" aria-valuemin="0"
                             class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"></div>
                    </div>
                    <br> <!-- fieldsets -->
                    <fieldset data-position="1">
                        <div class="form-card">
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">Personal Information:</h2>
                                </div>
                                <div class="col-5">
                                    <h2 class="steps">Step 1 - 4</h2>
                                </div>
                            </div>
                            <label class="fieldlabels">First Name: *</label>
                            <input name="first_name" placeholder="First Name" type="text"/>

                            <label class="fieldlabels">Last Name: *</label>
                            <input name="last_name" placeholder="Last Name" type="text"/>

                            <label class="fieldlabels">Telephone: *</label>
                            <input name="telephone" placeholder="Telephone" type="text"/>
                        </div>
                        <input class="next action-button" name="next" type="button" value="Next"/>
                    </fieldset>
                    <fieldset data-position="2">
                        <div class="form-card">
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">Address Information:</h2>
                                </div>
                                <div class="col-5">
                                    <h2 class="steps">Step 2 - 4</h2>
                                </div>
                            </div>

                            <label class="fieldlabels">Street: *</label>
                            <input name="address_street" placeholder="Street" type="text"/>

                            <label class="fieldlabels">Number: *</label>
                            <input name="address_number" placeholder="Number" type="text"/>

                            <label class="fieldlabels">City: *</label>
                            <input name="address_city" placeholder="City" type="text"/>

                            <label class="fieldlabels">State: *</label>
                            <input name="address_state" placeholder="State" type="text"/>

                            <label class="fieldlabels">Zip code: *</label>
                            <input name="address_zip" placeholder="ZIP Code" type="text"/>
                        </div>
                        <input class="next action-button" name="next" type="button" value="Next"/> <input
                        class="previous action-button-previous"
                        name="previous"
                        type="button"
                        value="Previous"/>
                    </fieldset>
                    <fieldset data-position="3">
                        <div class="form-card">
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">Payment Information:</h2>
                                </div>
                                <div class="col-5">
                                    <h2 class="steps">Step 3 - 4</h2>
                                </div>
                            </div>
                            <label class="fieldlabels">Account owner: *</label>
                            <input name="owner" placeholder="Account owner" type="text"/>

                            <label class="fieldlabels">IBAN: *</label>
                            <input name="iban" placeholder="IBAN" type="text"/>
                        </div>
                        <input class="action-button submit" name="next" type="button" value="Submit"/>
                        <input class="previous action-button-previous" name="previous" type="button" value="Previous"/>
                    </fieldset>
                    <fieldset data-position="4">
                        <div class="form-card">
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">Finish:</h2>
                                </div>
                                <div class="col-5">
                                    <h2 class="steps">Step 4 - 4</h2>
                                </div>
                            </div>
                            <br><br>
                            <h2 class="purple-text text-center"><strong>SUCCESS !</strong></h2> <br>
                            <div class="row justify-content-center">
                                <div class="col-3"><img class="fit-image" src="https://i.imgur.com/GwStPmg.png"></div>
                            </div>
                            <br><br>
                            <div class="row justify-content-center">
                                <div class="col-7 text-center">
                                    <h5 class="purple-text text-center">You Have Successfully Signed Up</h5>
                                    <p class="purple-text text-center message"></p>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    const sessionKey = '<?php echo $sessionKey ?>';
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js?bust=<?php echo $bust ?>"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js?bust=<?php echo $bust ?>"></script>
<script src="assets/js/scripts.js?bust=<?php echo $bust ?>"></script>
</body>
</html>





