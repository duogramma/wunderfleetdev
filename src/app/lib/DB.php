<?php
    /**
     * Created by PhpStorm.
     * User: jacksonfdam
     * Date: 2021-04-12
     * Time: 00:46
     *
     * Based on https://github.com/lodev09/php-models/blob/master/src/DB.php
     */
    
    namespace App\Lib;
    
    class DB extends \PDO
    {
        private $error;
        private $sql;
        private $bind;
        private $error_callback;
        private $prefix;
    
        private $host;
        private $database;
        private $username;
        private $password;
    
        const TYPE_STRING = 'string';
        const TYPE_INT = 'int';
        const TYPE_BOOL = 'bool';
        const TYPE_FLOAT = 'float';
        const TYPE_DATETIME = 'datetime';
        const TYPE_DATE = 'date';
        const TYPE_SPATIAL = 'spatial';
    
        /**
         * Class constructor.
         *
         * @param string $host
         * @param string $database
         * @param string $username
         * @param string $password
         * @param int $port
         * @param string $driver
         * @param null $pdo_options
         */
        public function __construct($host = '', $database = '', $username = '', $password = '', $port = 3306, $driver = 'mysql', $pdo_options = null) {
            $options = [
                \PDO::ATTR_PERSISTENT => true,
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
            ];
        
            if ($pdo_options) {
                if (!is_array($pdo_options)) throw new \PDOException('PDO options should be an array');
                $options = array_merge($options, $pdo_options);
            }
        
            $this->host = $host;
            $this->database = $database;
            $this->username = $username;
            $this->password = $password;
        
            switch ($driver) {
                case 'mysql':
                    $dsn = "mysql:host=$host;port=$port;dbname=$database";
                    break;
            }
        
            parent::__construct($dsn, $username, $password, $options);
        }
    
        public function is($sql, $what) {
            $what = is_array($what) ? $what : [$what];
            return preg_match('/^\s*('.implode('|', $what).')\s+/i', $sql) ? true : false;
        }
    
        /**
         * INSERT statement.
         *
         * @param string $table
         *  Table name or INSERT statement
         *
         * @param array $data
         *  Associative array with field names and values
         *
         * @return array|bool|int
         *  If no SQL errors are produced, this method will return with the the last
         *  inserted ID. Otherwise 0.
         */
        public function insert($sql = '', $data = []) {
            if ($this->is($sql, 'insert')) {
                return $this->run($sql, $data);
            } else {
                $table = $this->prefix.$sql;
                $data_fields = array_keys($data);
            
                $sql = "INSERT INTO `$table` (`".implode("`, `", $data_fields)."`)";
            
                $fields = $this->getFields($table);
            
                $bind = [];
                $values = [];
            
                foreach ($data_fields as $field) {
                    if (isset($fields[$field])) {
                        $type = $fields[$field]['type'];
                    
                        switch ($type) {
                            // direct value inject
                            case self::TYPE_SPATIAL:
                                $values[] = $data[$field];
                                break;
                        
                            default:
                                $bind[":$field"] = $data[$field];
                                $values[] = ":$field";
                                break;
                        }
                    }
                }
            
                $sql .= " VALUES (".implode(", ", $values).")";
                return $this->run($sql, $bind);
            }
        }
    
        /**
         * Allias of run
         */
        public function select($sql, $bind = null, $args = null, $style = null) {
            return $this->run($sql, $bind, $args, $style);
        }
    
        /**
         * UPDATE statement.
         *
         * @param string $sql
         *  Table name.
         *
         * @param array $info
         *  Associated array with fields and their values.
         *
         * @param string $where
         *  WHERE conditions.
         *
         * @param mixed $bind
         *  Bind parameters as string or array.
         *
         * @return array|bool|int
         *  If no SQL errors are produced, this method will return the number of rows
         *  affected by the UPDATE statement.
         */
        public function update($sql = '', $data = [], $where = '', $bind = '') {
            if ($this->is($sql, 'update')) {
                return $this->run($sql, $data);
            } elseif (is_array($data)) {
                $table = $this->prefix.$sql;
                $data_fields = array_keys($data);
                $sql = "UPDATE $table";
            
                $fields = $this->getFields($table, $data_fields);
            
                $set_fields = [];
                $bind = $this->cleanup($bind);
                foreach ($data_fields as $field) {
                    if (isset($fields[$field])) {
                        $type = $fields[$field]['type'];
                        switch ($type) {
                            // direct value inject
                            case self::TYPE_SPATIAL:
                                $set_fields[] = "`$field` = $data[$field]";
                                break;
                            default:
                                $set_fields[] = "`$field` = :update_$field";
                                $bind[":update_$field"] = $data[$field];
                                break;
                        }
                    }
                }
            
                $sql .= " SET ".implode(", ", $set_fields);
                if ($where) $sql .= " WHERE $where";
            
                return $this->run($sql, $bind);
            } else {
                $this->error = 'Invalid update parameters';
                $this->debug();
                return false;
            }
        }
    
        /**
         * DELETE statement.
         *
         * @param string $sql
         *  Table name or DELETE statement
         *
         * @param string $info
         *  Where conditions or bind information
         *
         * @param mixed $bind
         *  Bind parameters as string or array.
         *
         * @return array
         *  If no SQL errors are produced, this method will return the number of rows
         *  affected by the DELETE statement.
         */
        public function delete($sql_or_table = '', $info = '', $bind = '') {
            if ($this->is($sql_or_table, 'delete')) {
                return $this->run($sql_or_table, $info);
            } else {
                $sql = $this->prefix.$sql_or_table;
                $sql = "DELETE FROM $sql WHERE $info;";
                return $this->run($sql, $bind);
            }
        }
    
        /**
         * Fetch a single row
         *
         * @param string $sql
         *  Query string
         *
         * @param mixed $bind
         *  Bind parameters as string or array
         *
         * @param mixed $args
         *  Additional arguments for fetch
         *
         * @param long $style
         *  Fetch style
         *
         * @return mixed
         * If no SQL errors are procuded, this method will return the object. Otherwise returns false.
         */
        public function row($sql, $bind = null, $args = null, $style = null) {
            $smt = $this->stmt($sql, $bind, $args, $style);
            if ($smt !== false) {
                if (isset($args)) {
                    $smt->setFetchMode($style ?: \PDO::FETCH_CLASS, $args);
                } else {
                    $smt->setFetchMode($style ?: \PDO::FETCH_OBJ);
                }
            
                return $smt->fetch();
            }
        
            return false;
        }
    
        /**
         * This method is used to run free-form SQL statements
         *
         * @param string $sql
         *  SQL query.
         *
         * @param mixed $bind
         *  Bind parameters as string or array.
         *
         * @param mixed $style
         *  Fetch style
         *
         * @param mixed $args
         *  Additional arguments
         *
         * @return array|bool|int
         *  If no SQL errors are produced, this method will return the number of
         *  affected rows for DELETE and UPDATE statements, the last inserted ID for
         *  INSERT statement, or an associate array of results for SELECT, DESCRIBE,
         *  and PRAGMA statements. Otherwise false.
         */
        public function run($sql = '', $bind = null, $args = null, $style = null) {
            $smt = $this->stmt($sql, $bind, $args, $style);
            if ($smt !== false) {
                if ($this->is($this->sql, ['delete', 'update'])) {
                    return $smt->rowCount();
                } elseif ($this->is($this->sql, 'insert')) {
                    return $this->lastInsertId();
                } else {
                    if (isset($args)) {
                        return $smt->fetchAll($style ?: \PDO::FETCH_CLASS, $args);
                    } else {
                        return $smt->fetchAll($style ?: \PDO::FETCH_OBJ);
                    }
                }
            }
        
            return false;
        }
    
        /**
         * Execute and return the statement
         */
        public function stmt($sql = '', $bind = null, $args = null, $style = null) {
            $this->sql = trim($sql);
            $this->bind = $this->cleanup($bind);
            $this->error = '';
        
            try {
                $smt = $this->prepare($this->sql);
                $smt->execute($this->bind);
            
                return $smt;
            } catch (\PDOException $e) {
                $this->error = $e->getMessage();
                $this->debug();
                return false;
            }
        
            return false;
        }
    
        /**
         * When a SQL error occurs, this project will send an error message to a
         * callback function specified through the onError method.
         * The callback function's name should be supplied as a string without
         * parenthesis.
         *
         * If no SQL errors are produced, this method will return an associative
         * array of results.
         *
         * @param $error_callback
         *  Callback function.
         */
        public function onError($callback) {
            if (is_string($callback)) {
                // Variable functions for won't work with language constructs such as echo
                // and print, so these are replaced with print_r.
                if (in_array(strtolower($callback), ['echo', 'print'])) {
                    $callback = 'print_r';
                }
            
                if (function_exists($callback)) {
                    $this->error_callback = $callback;
                }
            } else {
                $this->error_callback = $callback;
            }
        }
    
        public function getQuery() {
            $info = $this->getInfo();
            $query = $info['statement'];
        
            if (!empty($info['bind'])) {
                foreach ($info['bind'] as $field => $value) {
                    $query = str_replace(':'.$field, $this->quote($value), $query);
                }
            }
        
            return $query;
        }
    
        public function getInfo() {
            $info = [];
        
            if (!empty($this->sql)) {
                $info['statement'] = $this->sql;
            }
        
            if (!empty($this->bind)) {
                $info['bind'] = $this->bind;
            }
        
            return $info;
        }
    
        /**
         * Debug.
         */
        private function debug() {
            if (!empty($this->error_callback)) {
                $error = ['Error' => $this->error];
            
                if (!empty($this->sql)) {
                    $error['SQL Statement'] = $this->sql;
                }
            
                if (!empty($this->bind)) {
                    $error['Bind Parameters'] = trim(print_r($this->bind, true));
                }
            
                $backtrace = debug_backtrace();
                if (!empty($backtrace)) {
                    $backtraces = [];
                    foreach ($backtrace as $info) {
                        if (isset($info['file']) && $info['file'] != __FILE__) {
                            $backtraces[] = $info['file'] . ' at line ' . $info['line'];
                        }
                    }
                
                    if ($backtraces) {
                        $error['Backtrace'] = implode(PHP_EOL, $backtraces);
                    }
                }
            
                $msg = 'SQL Error' . PHP_EOL . str_repeat('-', 50);
                foreach ($error as $key => $val) {
                    $msg .= PHP_EOL . PHP_EOL . $key . ':' . PHP_EOL . $val;
                }
            
                $func = $this->error_callback;
                $func(new \PDOException($msg));
            }
        }
    
        /**
         * Map a datatype based upon the given driver
         *
         * @return string
         * Returns the equevalent php type
         */
        private function mapDataType($driver_type) {
            $map = [];
            $driver = $this->getAttribute(\PDO::ATTR_DRIVER_NAME);
            switch ($driver) {
                case 'mysql':
                    $map = [
                        self::TYPE_INT => ['smallint', 'mediumint', 'int', 'bigint'],
                        self::TYPE_BOOL => ['tinyint'],
                        self::TYPE_FLOAT => ['float', 'double', 'decimal'],
                        self::TYPE_DATETIME => ['datetime'],
                        self::TYPE_DATE => ['date'],
                        self::TYPE_SPATIAL => ['point', 'geometry', 'polygon', 'multipolygon', 'multipoint']
                    ];
                
                    break;
                case 'sqlite':
                    $map = [
                        self::TYPE_INT => ['integer'],
                        self::TYPE_FLOAT => ['real']
                    ];
            }
        
            foreach ($map as $type => $driver_types) {
                if (in_array(strtolower($driver_type), $driver_types)) {
                    return $type;
                    break;
                }
            }
        
            return self::TYPE_STRING;
        }
    
        /**
         * Get table info
         * @param  string $table
         * The table name
         *
         * @return array
         * Returns array of field information about the table
         */
        public function getFields($table) {
            $table = $this->prefix . $table;
            $driver = $this->getAttribute(\PDO::ATTR_DRIVER_NAME);
            $fields = [];
        
            if ($driver == 'sqlite') {
                $sql = "PRAGMA table_info('$table');";
            } else {
                $sql = "SELECT column_name AS name, data_type AS type, IF(column_key = 'PRI', 1, 0) AS pk FROM information_schema.columns";
                $sql .= " WHERE table_name = '$table' AND table_schema = '$this->database';";
            }
        
            if ($data = $this->run($sql)) {
                foreach ($data as $column_info) {
                    $field_info = [
                        'type' => $this->mapDataType($column_info->type),
                        'primary' => !empty($column_info->pk)
                    ];
                
                    $fields[$column_info->name] = $field_info;
                }
            }
        
            return $fields;
        }
    
        /**
         * Cleanup parameters.
         *
         * @param mixed $bind
         *  Bind parameters as string/array.
         *
         * @return array
         *  Bind parameters as array.
         */
        private function cleanup($bind = '') {
            if (!is_array($bind)) {
                if (!empty($bind)) {
                    $bind = [$bind];
                } else {
                    $bind = [];
                }
            }
        
            return $bind;
        }
    }
