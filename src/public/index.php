<?php
    require __DIR__ . '/../vendor/autoload.php';
    
    use App\Controller\HomeController;
    use App\Controller\UserController;
    use App\Lib\App;
    use App\Lib\Request;
    use App\Lib\Response;
    use App\Lib\Router;
    use App\Lib\Throttle;
    
    
    try {
        Throttle::check();
    } catch (\Exception $e){
        header('HTTP/1.1 503 Service Unavailable');
        die($e->getMessage());
    }
    
    Router::get('/', function () {
        (new HomeController())->indexAction();
    });
    
    Router::get('/users', function (Request $req, Response $res) {
        (new UserController())->indexAction($req, $res);
    });
    
    Router::post('/save', function (Request $req, Response $res) {
        (new UserController())->saveAction($req, $res);
        
    });
    
    App::run();
