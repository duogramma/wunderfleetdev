<?php
    /**
     * Created by PhpStorm.
     * User: jacksonfdam
     * Date: 2021-04-12
     * Time: 00:46
     */
    
    namespace App\Lib;
    
    class App
    {
        public $container;
        
        /**
         * App constructor.
         * @param $container
         */
        public function __construct()
        {
            $this->container = new Container();
        }
        
        
        public static function run()
        {
            Logger::enableSystemLogs();
            /*
             * TODO:Implements the DI
            $container = new Container();
            */
            
        }
    }
