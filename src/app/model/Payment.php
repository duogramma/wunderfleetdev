<?php
    /**
     * Created by PhpStorm.
     * User: jacksonfdam
     * Date: 2021-04-12
     * Time: 01:48
     */
    
    namespace App\Model;
    
    use App\Lib\Model;

    class Payment extends Model
    {
        public $customerId;
        public $owner;
        public $iban;
        public $paymentDataId;
        public $updated_at;
        public $created_at;
    }
