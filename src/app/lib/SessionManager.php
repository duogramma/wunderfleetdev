<?php
    /**
     * Created by PhpStorm.
     * User: jacksonfdam
     * Date: 2021-04-12
     * Time: 13:00
     */
    
    namespace App\Lib;
    
    use App\Domain\Repositories\SessionInterface;
    
    class SessionManager implements SessionInterface
    {
        public function __construct(string $cacheExpire = null, string $cacheLimiter = null)
        {
            if (session_status() === PHP_SESSION_NONE) {
                
                if ($cacheLimiter !== null) {
                    session_cache_limiter($cacheLimiter);
                }
                
                if ($cacheExpire !== null) {
                    session_cache_expire($cacheExpire);
                }
                
                session_start();
            }
        }
        
        /**
         * @param string $key
         * @param mixed $value
         * @return SessionManager
         */
        public function set(string $key, $value): SessionInterface
        {
            $_SESSION[$key] = $value;
            return $this;
        }
        
        public function remove(string $key): void
        {
            if (array_key_exists($key, $_SESSION)) {
                unset($_SESSION[$key]);
            }
        }
        
        public function clear(): void
        {
            session_unset();
        }
        
        public function has(string $key): bool
        {
            return array_key_exists($key, $_SESSION);
        }
        
        public function sessionId(): string
        {
            if (empty($_SESSION['code']) || time() - $_SESSION['code_time'] > 3600) {
                self::regenerate();
            }
            return self::get("code");
        }
        
        public function regenerate(): void
        {
            $_SESSION['code'] = uniqid();
            $_SESSION['code_time'] = time();
        }
        
        /**
         * @param string $key
         * @return mixed
         */
        public function get(string $key)
        {
            if (array_key_exists($key, $_SESSION)) {
                return $_SESSION[$key];
            }
            return null;
        }
}
