jQuery(document).ready(function () {


    let current_fs, next_fs, previous_fs;
    let opacity;
    if (!localStorage.current) localStorage.current = 1;
    let current = localStorage.current || 1;
    let steps = jQuery("fieldset").length;

    setProgressBar(current);

    function persist(event) {
        localStorage.setItem(event.target.name, event.target.value);
    }

    function redraw() {
        document.querySelectorAll("input[type='text']").forEach((inputEl) => {
            inputEl.value = localStorage.getItem(inputEl.name);
            inputEl.addEventListener("change", persist);
        });

        if (current > 1) {
            for (let i = 1; i < current; i++) {
                jQuery([...jQuery("#progressbar li")][i]).addClass("active");
            }

            setProgressBar(current);
            jQuery([...jQuery("fieldset")]).hide().css({
                'display': 'none',
                'position': 'relative',
                'opacity': 0
            });

            jQuery([...jQuery("fieldset")][current - 1]).show().css({
                'display': 'block',
                'position': 'relative',
                'opacity': 1
            });
        }
    }

    redraw();

    jQuery(".next").click(function () {

        current_fs = jQuery(this).parent();
        next_fs = jQuery(this).parent().next();

        //Add Class Active
        jQuery("#progressbar li").eq(jQuery("fieldset").index(next_fs)).addClass("active");

        //show the next fieldset
        next_fs.show();

        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function (now) {

                // for making fielset appear animation
                opacity = 1 - now;

                current_fs.css({
                    'display': 'none',
                    'position': 'relative'
                });
                next_fs.css({'opacity': opacity});
            },
            duration: 500
        });
        setProgressBar(++current);
    });

    jQuery(".previous").click(function () {

        current_fs = jQuery(this).parent();
        previous_fs = jQuery(this).parent().prev();

        //Remove class active
        jQuery("#progressbar li").eq(jQuery("fieldset").index(current_fs)).removeClass("active");

        //show the previous fieldset
        previous_fs.show();

        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function (now) {
                // for making fielset appear animation
                opacity = 1 - now;

                current_fs.css({
                    'display': 'none',
                    'position': 'relative'
                });
                previous_fs.css({'opacity': opacity});
            },
            duration: 500
        });
        setProgressBar(--current);
    });

    function setProgressBar(curStep) {
        localStorage.current = curStep;
        let percent = parseFloat(100 / steps) * curStep;
        percent = percent.toFixed();
        jQuery(".progress-bar")
            .css("width", percent + "%")
    }

    jQuery(".submit").click(function () {

        let data = {};
        document.querySelectorAll("input[type='text']").forEach((inputEl) => {
            data[inputEl.name] = inputEl.value
        });
        $.ajax({
            type: "POST",
            url: window.location.href + "save",
            headers: {
                'sessionKey': sessionKey
            },
            data: JSON.stringify(data),
            contentType: "application/json",
            dataType: "json",
            success: function (data) {

                if (data.customerId != null) {
                    console.log(data);
                    localStorage.current = ++current;
                    localStorage.customerId = data.customerId;
                    document.querySelector('p.message').textContent = data.customerId;
                    redraw();
                    localStorage.clear()
                }
            },
            error: function (errMsg) {
                console.error(errMsg);
            }
        });

        return false;
    })

});
