<?php
    /**
     * Created by PhpStorm.
     * User: jacksonfdam
     * Date: 2021-04-12
     * Time: 09:55
     */
    
    namespace App\Services;

    use App\Domain\Abstractions\AbstractDomainService;
    use App\Infrastructure\Persistence\Repositories\UserRepository;
    use DateTime;
    use DateTimeZone;
    use GuzzleHttp\Client;
    use GuzzleHttp\Exception\ClientException;
    
    class UserService extends AbstractDomainService
    {
        private $userRepository;
        
        /**
         * UserService constructor.
         */
        public function __construct()
        {
            $this->userRepository = new UserRepository();
        }
        
        public function findAll()
        {
            return $this->userRepository->findAll();
        }
        
        
        /**
         * @param $entity
         * @return \Exception|mixed
         */
        public function save($entity)
        {
            try {
                $dataToArray = json_decode(json_encode($entity), true);
                if(!isset($dataToArray) || empty($dataToArray)){
                    throw new Exception("Error saving user");
                }
                return $this->userRepository->save($dataToArray);
            } catch (\Exception $e) {
                return $e;
            }
            
            
        }
        
        public function sendToServer($entity)
        {
            $client = new Client([
                'headers' => ['Content-Type' => 'application/json']
            ]);
            $postData = new \stdClass();
            $postData->customerId = (int)$entity->customerId;
            $postData->owner = $entity->owner;
            $postData->iban = $entity->iban;
            
            try {
                $response = $client->request('POST',
                    'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data',
                    ['body' => json_encode($postData), 'timeout' => 15]
                );
                $data = json_decode($response->getBody()->getContents());
                
                if(isset($data->paymentDataId)) {
                    $postData->paymentDataId = $data->paymentDataId;
    
                    $now = new DateTime(null, new DateTimeZone('Europe/London'));
                    $postData->created_at = $now->format('Y-m-d H:i:s');
                    $postData->updated_at = $now->format('Y-m-d H:i:s');
                    return $this->userRepository->savePayment(json_decode(json_encode($postData), true));
                } else {
                    throw new Exception("Error saving $data->paymentDataId");
                }
                
            } catch (ClientException $e) {
                $response = $e->getResponse();
                return json_decode($response->getBody()->getContents());
            }
        }
}
