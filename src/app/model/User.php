<?php
    /**
     * Created by PhpStorm.
     * User: jacksonfdam
     * Date: 2021-04-12
     * Time: 01:48
     */
    
    namespace App\Model;
    
    use App\Lib\Model;

    class User extends Model
    {
        public $customerId;
        public $first_name;
        public $last_name;
        public $telephone;
        public $address_street;
        public $address_number;
        public $address_city;
        public $address_state;
        public $address_zip;
        public $owner;
        public $iban;
    }
