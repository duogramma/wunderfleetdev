<?php
    
    $env = __DIR__ . '/../.env';
    
    $dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/');
    $dotenv->load();
    
    $dotenv->required([
        'DB_CONNECTION',
        'DB_HOST',
        'DB_PORT',
        'DB_DATABASE',
        'DB_USERNAME',
        'DB_PASSWORD'
    ]);
    
    return [
        'LOG_PATH' => __DIR__ . '/logs',
        'VIEW_PATH' => __DIR__ . '/resources/views/',
        'CACHE_PATH' => __DIR__ . '/resources/cache/',
        'DB_HOST' => $_ENV['DB_HOST'],
        'DB_DATABASE' => $_ENV['DB_DATABASE'],
        'DB_USERNAME' => $_ENV['DB_USERNAME'],
        'DB_PASSWORD' => $_ENV['DB_PASSWORD'],
        'DB_PORT' => $_ENV['DB_PORT'],
        'REDIS_PORT' => $_ENV['REDIS_PORT'],
        'REDIS_SERVER' => $_ENV['REDIS_SERVER'],
    ];
