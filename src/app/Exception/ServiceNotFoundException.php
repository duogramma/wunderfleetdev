<?php
    /**
     * Created by PhpStorm.
     * User: jacksonfdam
     * Date: 2021-04-12
     * Time: 10:19
     */
    
    namespace App\Exception;

    class ServiceNotFoundException extends \Exception
    {
    }
