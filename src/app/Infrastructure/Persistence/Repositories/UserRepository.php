<?php
    /**
     * Created by PhpStorm.
     * User: jacksonfdam
     * Date: 2021-04-12
     * Time: 09:53
     */
    
    namespace app\Infrastructure\Persistence\Repositories;

    use App\Domain\Repositories\UserRepositoryInterface;
    use App\Lib\Config;
    use App\Lib\DB;
    use App\Lib\Model;
    use App\Model\Payment;
    use App\Model\User;

    class UserRepository implements UserRepositoryInterface
    {
        /**
         * UserRepository constructor.
         */
        public function __construct()
        {
            try {
                $db = new DB(
                    Config::get('DB_HOST', ''),
                    Config::get('DB_DATABASE', ''),
                    Config::get('DB_USERNAME', ''),
                    Config::get('DB_PASSWORD', ''),
                    Config::get('DB_PORT', '')
                );
                
                Model::setDb($db);
                User::register('user');
                Payment::register('payment');
                
            } catch (\Exception $e) {
                var_dump($e->getMessage());
            }
        }
        
        /**
         * @return mixed
         */
        public function findAll()
        {
            return User::row("SELECT *  FROM user");
        }
    
        /**
         * @param array $entity
         * @return mixed
         * @throws \Exception
         */
        public function save($entity = []):User
        {
            return User::insert($entity);
        }
        
        /**
         * @param array $entity
         * @return mixed
         * @throws \Exception
         */
        public function savePayment($entity = []): Payment
        {
            return Payment::insert($entity);
        }
}
