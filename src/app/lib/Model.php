<?php
    /**
     * Created by PhpStorm.
     * User: jacksonfdam
     * Date: 2021-04-12
     * Time: 00:58
     */
    
    namespace App\Lib;

    class Model
    {
        public static $db;
        public static $app;
        public static $debug = false;
        
        /**
         * Store registered models globally
         */
        private static $models = [];
    
        /**
         * Debug helper
         *
         * @param  string $msg
         * The exception
         *
         * @return bool
         * Returns false by default
         * @throws \Exception
         */
        private static function error($msg)
        {
            if (self::$debug) {
                throw new \Exception($msg);
            } else {
                trigger_error($msg, E_USER_NOTICE);
                return false;
            }
        }
        
        /**
         * Get the name of the child class
         *
         * @return string
         * The name of the child class
         */
        public static function getModelClassName()
        {
            return get_called_class();
        }
    
        /**
         * Get a single object. If arg 2 is provided, then it's assumed that arg 1 is a field :)
         * @param  mixed $arg1
         * Could be a field name or a value or array filter
         *
         * @param  string $arg2
         * Value if arg1 is field
         *
         * @return object
         * Returns the object
         * @throws \Exception
         */
        public static function instance($arg1, $arg2 = null)
        {
            $model = self::getModelInfo();
            if (!$model) {
                return false;
            }
            
            $table = $model['table'];
            
            if (isset($arg2)) {
                $filter_str = "`$arg1` = :value";
                $binds = [':value' => $arg2];
            } else {
                if ($arg1 instanceof self) {
                    return $arg1;
                }
                
                if (is_array($arg1)) {
                    $filter_str = self::createFilter($arg1, $binds, null);
                } else {
                    $pk = $model['pk'];
                    $filter_str = "`$pk` = :value";
                    $binds = [':value' => $arg1];
                }
            }
            
            if (self::getField('active')) {
                $filter_str = 'active = 1 AND ' . $filter_str;
            }
            
            return self::row("SELECT * FROM `$table` WHERE $filter_str", $binds);
        }
        
        public static function connect($host, $database, $username, $password)
        {
            $db = new DB($host, $database, $username, $password);
            self::setDb($db);
        }
    
        /**
         * Set the DB object to our base model class
         *
         * @param $db
         * The DB instance
         * @return bool
         * @throws \Exception
         */
        public static function setDb($db)
        {
            if ($db) {
                self::$db = $db;
                return true;
            }
            
            return self::error('db is not valid');
        }
        
        /**
         * Get the DB object from the base Model
         *
         * @return DB
         */
        public static function getDb()
        {
            return self::$db;
        }
    
        /**
         * Register a model for the base class to use proper property data types and some other preps in the future
         *
         * @return bool
         * Returns true if table exists and successfully registered to the Model class. Otherwise false.
         * @throws \Exception
         */
        public static function register($table, $pk = null)
        {
            if ($fields = self::$db->getFields($table)) {
                if (!$pk) {
                    foreach ($fields as $field => $info) {
                        if ($info['primary'] === true) {
                            $pk = $field;
                            break;
                        }
                    }
                }
                
                self::$models[self::getModelClassName()] = [
                    'fields' => $fields,
                    'table' => $table,
                    'pk' => $pk
                ];
                
                return true;
            }
            
            return self::error('table "' . $table . '" for ' . self::getModelClassName() . ' not found in database.');
        }
    
        /**
         * Given that the model class is registered, this helper class gets information about the model stored in
         * the static $_models property
         *
         * @return array
         * Returns the array of model information
         * @throws \Exception
         */
        private static function getModelInfo()
        {
            $class_name = self::getModelClassName();
            if (isset(self::$models[$class_name])) {
                return self::$models[$class_name];
            }
            
            return self::error(self::getModelClassName() . ' must be registered with it\'s corresponding table name and database. Use \Models\\' . self::getModelClassName() . '::register($db, $table)');
        }
    
        /**
         * Create an array given a field from the data set
         *
         * @param $field
         * the field to map
         *
         * @return array|bool
         * @throws \Exception
         */
        public static function map($field = 'id', $filters = [])
        {
            $model = self::getModelInfo();
            if (!$model) {
                return false;
            }
            if (!self::getField($field)) {
                return false;
            }
            
            if ($filters && !empty($filters[0]) && $filters[0] instanceof self) {
                $data = $filters;
            } else {
                $table = $model['table'];
                $active_field = self::getField('active') ? "active = 1" : "";
                
                $filters_str = self::createFilter($filters, $binds);
                $data = self::select("SELECT $field FROM `$table` WHERE $active_field $filters_str", $binds);
            }
            
            if (!$data) {
                return [];
            }
            
            return array_map(function ($row) use ($field) {
                $value = is_array($row) ? $row[$field] : $row->{$field};
                self::setDataType($field, $value);
                
                return $value;
            }, $data);
        }
    
        /**
         * Sets the type of a property value
         *
         * @param $field
         * @param mixed $value
         * The value to be set
         * @return bool
         * @throws \Exception
         */
        private static function setDataType($field, &$value)
        {
            $model = self::getModelInfo();
            if (!$model) {
                return false;
            }
            if (is_null($value)) {
                return false;
            }
            
            if ($type = self::getDataType($field)) {
                switch ($type) {
                    case DB::TYPE_DATE:
                    case DB::TYPE_DATETIME:
                    case DB::TYPE_SPATIAL:
                        break;
                    default:
                        settype($value, $type);
                }
            }
            
            return $type;
        }
        
        public static function getDataType($field)
        {
            $model = self::getModelInfo();
            if (!$model) {
                return false;
            }
            
            if (isset($model['fields'][$field])) {
                $field_info = $model['fields'][$field];
                return $field_info['type'];
            }
            
            return false;
        }
    
        /**
         * Inherited static method to call DB::insert(...)
         * @param  array $data
         * The data to insert
         *
         * @return int
         * Returns the newly inserted ID
         * @throws \Exception
         */
        public static function insert($data)
        {
            $model = self::getModelInfo();
            if (!$model) {
                return false;
            }
            
            $table = $model['table'];
            
            $id = self::$db->insert($table, $data);
            return $id ? self::instance($id) : false;
        }
        
        /**
         * tidy mysql date values
         *
         * @param array $fields
         * list of date fields to clean
         *
         * @param mixed $data
         * data source
         *
         * @return mixed
         * returns cleaned data
         */
        public static function formatDates($fields, $data, $format = 'Y-m-d H:i:s')
        {
            foreach ($fields as $field) {
                if (is_array($data) && isset($data[$field])) {
                    $data[$field] = $data[$field] ? date($format, strtotime($data[$field])) : null;
                } else {
                    if (is_object($data) && isset($data->{$field})) {
                        $data->{$field} = $data->{$field} ? date($format, strtotime($data->{$field})) : null;
                    }
                }
            }
            
            return $data;
        }
    
        /**
         * Inherited static method to call DB::select(...)
         *
         * @param $sql
         * sql string
         *
         * @param $bind
         * Bind parameters as string or array
         *
         * @return
         */
        public static function select($sql, $bind = null)
        {
            return self::$db->select($sql, $bind, self::getModelClassName());
        }
    
        /**
         * Inherited static method to call DB::row(...)
         *
         * @param $sql
         * sql string
         *
         * @param $bind
         * Bind parameters as string or array
         *
         * @return
         */
        public static function row($sql, $bind = null)
        {
            return self::$db->row($sql, $bind, self::getModelClassName());
        }
    
        /**
         * Build filter string
         *
         * @param $info
         * Fields and values
         *
         * @param &$binds
         * Updated binds
         *
         * @param $prepend
         * WHERE or AND
         *
         * @return string
         */
        public static function createFilter($info, &$binds = [], $prepend = 'AND')
        {
            $filters = [];
            
            if (!$info) {
                return '';
            }
            if (is_string($info)) {
                $info = [$info];
            }
            
            foreach ($info as $field => $value) {
                if (is_int($field)) {
                    $filters[] = $value;
                } else {
                    $bind_key = str_replace(".", "_", $field);
                    
                    $field_info = explode('.', $field);
                    $table = isset($field_info[1]) ? $field_info[0] . '.' : '';
                    $field_name = $field_info[1] ?? $field_info[0];
                    
                    $filters[] = $table . "`$field_name` = :$bind_key";
                    $binds[$bind_key] = $value;
                }
            }
            
            return $prepend . ' ' . implode(' AND ', $filters);
        }
    
        /**
         * Inherited method to call DB::update(...). Updates the object with new data
         *
         * @param  mixed $arg1
         * array if multiple fields are proived. Could be string for a field name
         *
         * @param  mixed $arg2
         * The value of $arg1 is a field name
         *
         * @return int
         * Returns the number of rows affected
         * @throws \Exception
         */
        public function update($arg1, $arg2 = null)
        {
            if (!$arg1) {
                return false;
            }
            
            $model = self::getModelInfo();
            if (!$model) {
                return false;
            }
            
            $table = $model['table'];
            $pk = $model['pk'];
            $data = [];
            
            if (is_string($arg1)) {
                $data[$arg1] = $arg2;
            } else {
                $data = $arg1;
            }
            
            $updated = self::$db->update($table, $data, "$pk = :pk", [':pk' => $this->{$pk}]);
            if ($updated) {
                self::$db->row("SELECT * FROM `$table` WHERE $pk = :pk", [':pk' => $this->{$pk}], $this,
                    \PDO::FETCH_INTO);
            }
            
            return $updated;
        }
    
        /**
         * Get field info
         *
         * @param $field
         * Field name
         *
         * @return bool
         *
         * @throws \Exception
         */
        public static function getField($field)
        {
            $model = self::getModelInfo();
            if (!$model) {
                return false;
            }
            
            return isset($model['fields'][$field]) ? $model['fields'][$field] : false;
        }
    
        /**
         * Is field a primary key
         *
         * @param @field
         * Field name
         *
         * @return bool
         *
         * @throws \Exception
         */
        public static function isPK($field)
        {
            $field = self::getField($field);
            return $field ? $field['primary'] : false;
        }
    
        /**
         * Delete the model (deactivate). We are not actually deleting row but instead setting `active` = 1
         * (hence active field is required)
         *
         * @return int
         * Returns the number of rows affected from "update"
         * @throws \Exception
         */
        public function delete()
        {
            $model = self::getModelInfo();
            if (!$model) {
                return false;
            }
            
            $table = $model['table'];
            $pk = $model['pk'];
            
            // we don't delete here :P
            if (self::getField('active')) {
                $data = ['active' => 0];
                if (self::getField('deleted_at')) {
                    $data['deleted_at'] = date('Y-m-d H:i:s');
                }
                
                return $this->update($data);
            } else {
                return self::$db->delete("DELETE FROM $table WHERE $pk = :pk", [':pk' => $this->{$pk}]);
            }
        }
        
        /**
         * Used to properly set data type of each property.
         * This overload is disabled by default as it increases mapping (general output) to around 200%
         *
         * public function __set($name, $value) {
         *    $this->_setDataType($name, $value);
         *    $this->{$name} = $value;
         *}
         */
        
        /**
         * Update correct data type and JSON encode
         *
         * @param mixed $flags
         * JSON flags
         *
         * @return string
         * JSON string
         */
        public function json($flags = JSON_PRETTY_PRINT)
        {
            return json_encode($this->toArray(), $flags);
        }
        
        public function toArray($fields = null, $datetime_format = \DateTime::ISO8601)
        {
            $model = self::getModelInfo();
            if (!$model) {
                return false;
            }
            
            $result = [];
            $properties = get_object_vars($this);
            
            if (!$fields) {
                $fields = array_keys($properties);
            }
            
            foreach ($fields as $field) {
                
                $value = null;
                
                if (array_key_exists($field, $properties)) {
                    $value = $properties[$field];
                    $type = self::setDataType($field, $value);
                    
                    // format dates if specified
                    if ($datetime_format && ($type === DB::TYPE_DATETIME || in_array($field,
                                ['created_at', 'updated_at', 'deleted_at']))) {
                        $value = date($datetime_format, strtotime($value));
                    }
                    
                    $result[$field] = $value;
                }
                
                $result[$field] = $value;
            }
            
            return $result;
        }
    }
