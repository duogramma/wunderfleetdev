<?php
    /**
     * Created by PhpStorm.
     * User: jacksonfdam
     * Date: 2021-04-12
     * Time: 10:51
     */
    
    namespace App\Exception;

    use Exception;
    use Psr\Container\NotFoundExceptionInterface;
    
    /**
     * Class not found
     */
    class NotFoundException extends Exception implements NotFoundExceptionInterface
    {
    }
