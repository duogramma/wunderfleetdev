# Wunder Mobility
==================================

Simply, unzip the file, this will an file `docker-compose.yml` on the root and a folder named `docker` containing nginx and php-fpm config for it.

## How to run

Dependencies:

  * Docker engine v1.13 or higher. Your OS provided package might be a little old, if you encounter problems, do upgrade. See [https://docs.docker.com/engine/installation](https://docs.docker.com/engine/installation)
  * Docker compose v1.12 or higher. See [docs.docker.com/compose/install](https://docs.docker.com/compose/install/)

Once you're done, simply `cd` to wunderfleet and run `docker-compose up -d`. 

This will initialise and start all the containers, then leave them running in the background.

## Services exposed outside your environment

You can access your application via **`localhost`**, if you're running the containers directly, or through **``** when run on a vm. 

Nginx respond to any hostname, in case you want to add your own hostname on your `/etc/hosts` 

Service|Address outside containers
------|---------|-----------
Webserver|[localhost:8043](http://localhost:8043)


## Hosts within your environment

You'll need to configure your application to use any services you enabled:

Service|Hostname|Port number
------|---------|-----------
php-fpm|php-fpm|9000
Memcached|memcached|11211 (default)
Redis|redis|6379 (default)


![Infrastructure Architecture](docs/img/Screen_Shot_2021-04-13_at_15.57.59.png)

## Docker compose cheatsheet

**Note:** you need to cd first to where your docker-compose.yml file lives.

  * Start containers in the background: `docker-compose up -d`
  * Start containers on the foreground: `docker-compose up`. You will see a stream of logs for every container running.
  * Stop containers: `docker-compose stop`
  * Kill containers: `docker-compose kill`
  * View container logs: `docker-compose logs`
  * Execute command inside of container: `docker-compose exec SERVICE_NAME COMMAND` where `COMMAND` is whatever you want to run. Examples:
        * Shell into the PHP container, `docker-compose exec php-fpm bash`
        * Run symfony console, `docker-compose exec php-fpm bin/console`
        * Open a mysql shell, `docker-compose exec mysql mysql -uroot -pCHOSEN_ROOT_PASSWORD`


## Routes

> [POST] - /save

> Example data from https://randomuser.me/api/

**Request Body**

```json
{
    "first_name": "Madison",
    "last_name": "Anderson",
    "telephone": "+55 51 123456789",
    "address_street": "North Street",
    "address_number": "(343)-345-0652",
    "address_city": "Greymouth",
    "address_state": "West Coast",
    "address_zip": "34111",
    "owner": "Madison Anderson",
    "iban": "a07b193f-4f35-4369-985c-c0d3e5665780"
}

```
**Request Header**

```
 'sessionKey' = sessionKey
```
### Database

Inside `./docker/mysql` folder there is an `setup.sql` file that, starts with the mysql container.

#### User
![Payments](./docs/img/Screen_Shot_2021-04-13_at_15.56.52.png)

#### Payments
![Payments](./docs/img/Screen_Shot_2021-04-13_at_15.57.23.png)


### Infrastructure Architecture


![Infrastructure Architecture](./docs/img/Screen_Shot_2021-04-13_at_18.36.48.png)


### PHP_CodeSniffer


![PHP_CodeSniffer](./docs/img/Screen_Shot_2021-04-13_at_18.47.52.png)
![PHP_CodeSniffer](./docs/img/Screen_Shot_2021-04-13_at_18.55.36.png)


### Codeception (Needs Implements and improvements)


![Codeception](./docs/img/Screen_Shot_2021-04-13_at_18.58.21.png)

### Unit Tests (Needs Implements and improvements)


![Unit Tests](./docs/img/Screen_Shot_2021-04-13_at_18.59.03.png)

#### Anti DDOS steps:

    There is a long thread about it -> https://stackoverflow.com/a/14599129
    
> The applications restricts by only 03 request per second by IP.
    
  * The very first important thing is to identify the ddos attack first. Identifying the ddos attack more early means more better for your server .
  * Getting better bandwidth available for your server. Always keep more than enough bandwidth which is required to for your server. This won’t prevent DDOS attack but it will take longer time. By which you will get some extra time to act.
  * If you own your own web server then you can defend_at_network parameter by rate limit your router, add filters to drop packets to different sources of attacks, time out half opened connections more aggressively. Also set lower SYN, ICMP and UDP flood drop thresholds.
  * If you don’t have much idea about these things, then go and contact your hosting providers quickly. They can try their best prevent the DDOS attacks.
  * There are also Special DDOS mitigation service provided by Cloudflare and many other companies. By which they can help you to prevent the DDOS attacks. Also many companies offer cheap ddos protection and dos protection.


## Application Flow

All fields and progress are cached on localstorage, so user can close and open the
browser and no data is lost.

I have applyed an DDD pattern.

PSR's Used:
 - PSR-3:Logger Interface - https://www.php-fig.org/psr/psr-3/
 - PSR-4: Autoloader - https://www.php-fig.org/psr/psr-4/
 - PSR-7: HTTP message interfaces - https://www.php-fig.org/psr/psr-7/
 - PSR-11: Container interface - https://www.php-fig.org/psr/psr-11/
 - PSR-12: Extended Coding Style - https://www.php-fig.org/psr/psr-12/
 
### Step 01 - Personal Data


![Step 01](./docs/img/Screen_Shot_2021-04-13_at_15.53.57.png)

### Step 02 - Address Data

![Step 02](./docs/img/Screen_Shot_2021-04-13_at_15.54.25.png)

### Step 03 - Payment Data

![Step 03](./docs/img/Screen_Shot_2021-04-13_at_15.54.34.png)


### Step 04 - Confirmation

![Step 04](./docs/img/Screen_Shot_2021-04-13_at_15.56.12.png)

After the success response, all caches area clear.

### Needs to improve

 * Tests
 * Dependency Injection Container - I Started but it was taking some time to implements Providers and Managers.
 * Apply ORM / Or improve database and model classes.
 * Better router system (PSR-7: HTTP message interfaces and PSR-17: HTTP Factories)
 * PSR-15: HTTP Server Request Handlers 
 * PSR-16: Common Interface for Caching Libraries 