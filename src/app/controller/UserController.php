<?php
    /**
     * Created by PhpStorm.
     * User: jacksonfdam
     * Date: 2021-04-12
     * Time: 08:50
     */
    
    namespace App\controller;

    use App\Lib\Request;
    use App\Lib\Response;
    use App\Lib\SessionManager;
    use App\Services\UserService;

    class UserController
    {
        /**
         * @var Service
         */
        private $service;
        private $session;
        
        public function __construct()
        {
            $this->service = new UserService();
            $this->session = new SessionManager();
        }
        
        public function indexAction(Request $req, Response $res)
        {
            $res->toJSON($this->service->findAll());
        }
        
        public function saveAction(Request $req, Response $res)
        {
            $sessionKey = $this->getHeaders('sessionKey');
            if (!isset($sessionKey) || is_null($sessionKey)) {
                $res->status(401)->toJSON(['message'=> "Not authorized."]);
                die();
            }
            $data = $req->getJSON();
            $savedUser = $this->service->save($data);
            if (isset($savedUser) && isset($savedUser->customerId)) {
                $response = $this->service->sendToServer($savedUser);
                $res->status(201)->toJSON($response);
            }
            
        }
        
        function getHeaders($header_name = null)
        {
            $keys = array_keys($_SERVER);
            
            if (is_null($header_name)) {
                $headers = preg_grep("/^HTTP_(.*)/si", $keys);
            } else {
                $header_name_safe = str_replace("-", "_", strtoupper(preg_quote($header_name)));
                $headers = preg_grep("/^HTTP_${header_name_safe}$/si", $keys);
            }
            
            foreach ($headers as $header) {
                if (is_null($header_name)) {
                    $headervals[substr($header, 5)] = $_SERVER[$header];
                } else {
                    return $_SERVER[$header];
                }
            }
            
            return $headervals;
        }
    }
