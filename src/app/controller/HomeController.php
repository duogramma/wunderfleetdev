<?php namespace App\Controller;

use App\Lib\SessionManager;
use App\Lib\View;

class HomeController
{
    public function indexAction()
    {
        View::render("home.html", [
            "title" => "Wunder Mobility - Fleet",
            "bust" => "v202104121923",
            "sessionKey" => SessionManager::sessionId()
        ]);
    }
}
